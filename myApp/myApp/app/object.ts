export interface IObj {
    title: string;
    description: string;
}

export class Obj implements IObj
{
    title: string;
    description: string;

    constructor(title: string, descr: string){
        this.title=title;
        this.description=descr;
    }
}