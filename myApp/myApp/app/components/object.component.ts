import { Component, Input } from "@angular/core"

@Component({
    selector: 'obj-item',
    templateUrl: './app/components/object.component.html',
    
})

export class ObjItem {
    @Input() objects: Obj[];

}
 