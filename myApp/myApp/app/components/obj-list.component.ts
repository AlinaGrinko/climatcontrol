import { Component, Input, OnInit} from '@angular/core';
import {IObj, Obj } from '../../app/object';
import { ObjItem } from './object.component';
import { MyAppService } from '../shared/myapp.service';

@Component({
    selector: "obj-list",
    templateUrl : "./app/components/obj-list.component.html" ,
   // inputs: ['objests'] // привязка свойств через html шаблон
   directives: [ObjItem],
   providers:[ MyAppService ]       //регистрируем сервис
})                      

export class ObjListComponent implements OnInit {
   // @Input() objects: Obj[]; // импортированное через декоратор Input поле
    objects: IObj[]; //удалили импортирование после использования сервиса
  //  myAppService: MyAppService;

    constructor(private myAppService: MyAppService){
        this.objects=[];
    }
    ngOnInit(){
        this.myAppService.getObjects().then(objects=>this.objects=objects);
    }
    
}