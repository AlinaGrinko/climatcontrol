import { Component } from '@angular/core';
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { ObjListComponent } from './components/obj-list.component';
import { Obj } from './object'
import {objects} from './shared/myapp.data'

@Component({
    selector: 'myApp',
    templateUrl: './app/app.component.html',
    styleUrls: ['./bootstrap/css/bootstrap.css'],
    directives: [ ObjListComponent ] //указываем, что внутри компонента есть другой компонент
})
 
export class AppComponent {
title: string;
//objects: Obj[]; удалили после начала использования сервиса

constructor() {
    this.title="ClimatControl";
   // this.objects=objects;
  }
}

bootstrap(AppComponent);