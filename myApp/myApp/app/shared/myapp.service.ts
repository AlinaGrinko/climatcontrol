import { Injectable } from '@angular/core';
import { IObj } from 'C:/myApp/app/object';
import{ objects } from './myapp.data';

@Injectable() 

export class MyAppService {
    getObjects(): Promise<IObj[]> {
        return new Promise(resolve=> setTimeout(()=>resolve(objects), 1000));
    }
}