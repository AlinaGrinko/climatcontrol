import java.sql.*;

public class TestClass {
	private Connection con;
	private Statement st;
	private ResultSet rs;
	
	public TestClass() {
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/cc_v1", "root", "");
			st=con.createStatement();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void getData(){
		try{
			String query = "select * from object";
			rs=st.executeQuery(query);
			System.out.println("Records: ");
			while (rs.next()){
				String name = rs.getString("name");
				String descr = rs.getString("descr");
				System.out.println("Name: "+name+ " Description: "+descr);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
	}

}
