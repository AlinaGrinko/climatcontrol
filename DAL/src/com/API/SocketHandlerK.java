package com.API;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;

public class SocketHandlerK extends Thread {

	// ����� �� ������� �������, ������� ����������� ���������� � ��������
	private Socket socket;

	// ��� ������, ������� ��� ������� �������� (����� �������� ��������� ����)
	private Map<Socket, PrintWriter> sockets;

	public SocketHandlerK(Socket socket, Map<Socket, PrintWriter> sockets) throws IOException {
		this.socket = socket;
		this.sockets = sockets;
	}

	public void run() {
		String name = null;

		// �������� ������� ����� �� ������� "server <== client"
		// ��� ������ �� ����� try ������ ����� ����� ������������ ������
		try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

			// ������ �������� ���� ��� ������ �������, ��������� ���
			name = in.readLine();

			// ��������� ���� ���������, � ���, ��� ����� ������ ������������� �
			// �������; ��� ������� (������� ����, ��� �������������), � �����
			// ��� ������ � ����� �������� ��������� ������ ���������
			sendMessage("[Server] user <%s> is connected%n", true, name);

			// ���� ���������� � �������� �������, ������ �� ��� ����� ������
			String line = null;
			while ((line = in.readLine()) != null) {

				// � ��������� ���� ��������� ��������
				// �.�., ��� ������� �������� ���������
				sendMessage("%s%n", false, line);
			}

			// ���� ����������� �����, �� ���������� ���� ������� ��������
			// (������ ������ ����� �� ����� �������)
			// ��������� ���� ���������
			sendMessage("[Server] user <%s> is disconnected%n", true, name);

		} catch (IOException e) {

			// ���� ����������� �����, �� ��� ������, ��� ����� in.readLine()
			// �������� ����������
			// ������� - ������ ����� �� ������� �������, �.�. �������� ��������
			// ������� �� ���������
			// � ����� ������ ���������� ����� ������ ���������� ���� ������
			// ��������� �� ���� ���� ���������, ����� ���������� ������������
			// ������� ��������� ���� ����������
			sendMessage("[Server] user <%s> has been disconnected%n", true, name);

		}
	}

	// ��������� ��������� ���� ��������
	// (� ����� �������, ���� sendToServer =s true)
	private void sendMessage(String pattern, boolean sendToServer, Object... args) {
		// send to the server
		if (sendToServer) {
			System.out.printf(pattern, args);
		}
		// send to clients
			for (Socket socket : sockets.keySet()) {
				sockets.get(socket).printf(pattern, args);
			}

	}

}
