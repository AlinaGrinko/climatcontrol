package com.API;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
//
//import com.my.server.Server;
//import com.my.server.ShutdownServerListener;
//import com.my.server.SocketHandler;

public class ServerK {

	// ����� ����� ��������, �.�. ��������� �� ����� �� classpath
	// �� ����� ���������� (���������� � / - ��� ������, ��� ���������
	// � ����� classpath)
	//private static final String SETTINGS_FILE_ADDRESS = "/server.properties";

	// ���� �������
	private final int serverPort;

	// ������, ������� �������, ������ ��������� ������
	private String stopSignal;

	// ����, �� ������� ������� serverStopSignal �� Shutdown
	private int serverShutdownPort;

	// ���������, ������� �������� ��� �������� ������ � ��������� � ���� ������
	private Map<Socket, PrintWriter> sockets = new HashMap<>();
	
		public ServerK() throws IOException {
		// ��������� ���������
		
		serverPort = 55555;
		stopSignal = "stop";
		serverShutdownPort = 55557;
	}

	public void start() {
		// ��������� ��������� �����
		// ��� ������ �� ����� try �� ������������� ����� ������
		try (ServerSocket serverSocket = new ServerSocket(serverPort)) {

			// ����� � ������� �������
			System.out.printf("[Server] started (execute 'shutdown HOST %s %s' to stop)%n", serverShutdownPort,
					stopSignal);

			// ��������� ��������� �����, ������� ����� ������������ shutdown
			// ����
			new ShurtdownServerListenerK(serverSocket, serverShutdownPort, stopSignal).start();

			// ���� �� ������� � catch �� ����������, ��������� ��������
			while (true) {

				// ���� ����, �� ������������� ��������� ������
				// ������� ������ �� ���������� �� �������, ������� ������� ���
				// � ���������� �����
				// ���� �� ����� �������� ��������� ����� ����� ������ (���
				// ��������� ShutdownServerListener), �� ������ ����������
				// (������� �� �����)
				Socket clientSocket = serverSocket.accept();

				// �� ������ �������� ����� "server ==> client" � ������ �
				// ��������� sockets
				sockets.put(clientSocket, new PrintWriter(clientSocket.getOutputStream(), true));

				// ����� ������� � ��������� � ��������� ������ ����������
				// ������� ����������, ����� ���� ��������� � ����� ��������
				new SocketHandlerK(clientSocket, sockets).start();
			}
		} catch (IOException ex) {
			// ���� ��������, ���� ��������� ����� ��� ������ � ������
			// ShutdownServerListener, �� �����, ��� ������� ����� ���������
			// ����� serverSocket.accept()
			System.out.println("[Server] has been shut down");
		}

		// ��������� ������� (������ � ��������� � ���� ������), ����� ���� ����
		// �������� � ���, ��� ������ ����������
		closeResources();
	}

	private void closeResources() {
		// �������� �� ���� �������
		String request="";
		for (Socket socket : sockets.keySet()) {

			// �������� ����� ������ � ����� ��������� �� ��������� �������
			PrintWriter out = sockets.get(socket);
			try {
				BufferedReader inp = new BufferedReader(new InputStreamReader( socket.getInputStream() ));
				request=inp.readLine();
				execute(request);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			out.println("[Server] Server has been shut down, you should exit now...");

			// ��������� �������� �����
			out.close();

			// �����!!! ��� �������� ��������� ������ ����� ������� �������
			// ����� � ��� �����. �.�. ���������� ��������� ������ ��������
			// �����.

		}
	}
	
	public void execute(String request){
		String[] lines = request.split("/");
		if (lines[0].equals("Login")) {
			System.out.println("Got Login from" + lines[1]);
		}
		else
			System.out.println("not login");
	}

	public static void main(String[] args) throws IOException {

		// ��������� ������
		new ServerK().start();
	}

}
