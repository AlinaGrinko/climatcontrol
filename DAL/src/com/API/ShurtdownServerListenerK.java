package com.API;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

//������������� �����, ������� ������������ ����������� ���� (shutdown port)
//� � ������, ���� �� ���� �������� ������ �� ���������, ������������� ������
public class ShurtdownServerListenerK extends Thread {

	// ��������� ����� ��� ������ ������� �� Shutdown
	private ServerSocket serverSocket;

	// ������ �� ��������� �������
	private String stopSignal;

	// ����, ������� ����� ������������
	private int shutdownPort;

	public ShurtdownServerListenerK(ServerSocket serverSocket, int shutdownPort, String stopSignal) throws IOException {
		// ��������� ���������
		this.serverSocket = serverSocket;
		this.stopSignal = stopSignal;
		this.shutdownPort = shutdownPort;
	}

	public void run() {
		// ��������� ������������� ��������� ������� ��������� �����
		// ��� ������ �� ����� try ����� ����� ������
		try (ServerSocket shutdownSocket = new ServerSocket(shutdownPort)) {
			System.out.println("[ShutdownHandler] started");

			String line = null;
			// ���� �� ������� ����������� ������ �� ���������
			while (!stopSignal.equals(line)) {

				// ������� ������ �� ����������, ��� ������ ��� �����
				// �����������, �������� ������� ����� �� Shutdown
				// ��� ������ �� ����� try ������ ������� ����� ������� �
				// �������� �������
				try (Socket socket = shutdownSocket.accept();
						BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

					// ������ ������ ������ ����, ��� ������� ������ (Shutdown)
					line = in.readLine();
				}
			}

			// � ������ ����� ������ �� ��������� �������
			// ��������� ��������� �����, ��� ������ ������ ���������� �������
			// accept (��. ��� ������ Server: serverSocket.accept())
			serverSocket.close();

			System.out.println("[ShutdownHandler] stop signal has been obtained");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
