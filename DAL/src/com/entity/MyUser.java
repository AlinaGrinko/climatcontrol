package com.entity;

public class MyUser {
	private String name;
	private String login;
	private String password;
	
	public MyUser(){
		this.name="";
		this.login="";
		this.password="";
	}
	
	public MyUser(String name, String login, String password){
		this.name=name;
		this.login=login;
		this.password=password;
	}
	
	public String getName(){ return this.name;}
	public String getLogin(){ return this.login;}
	public String getPassword(){ return this.password;}
}
