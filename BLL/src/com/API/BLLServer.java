package com.API;

import java.net.*;

import java.io.*;

public class BLLServer {
	private static String fromDAL;

	public BLLServer() {
		fromDAL = "";
	}

	public BLLServer(String input) {
		fromDAL = input;
	}

	public static void main(String[] ar) {
		int port = 1939; // ��������� ���� (����� ���� ����� ����� �� 1025 ��
							// 65535)
		try {
			ServerSocket ss = new ServerSocket(port); // ������� ����� ������� �
														// ����������� ��� �
														// �������������� �����

			Socket socket = ss.accept(); // ���������� ������ ����� �����������
											// � ������� ��������� ����� ���-��
											// �������� � ��������
			// ����� ������� � �������� ������ ������, ������ ����� �������� �
			// �������� ������ �������.
			InputStream sin = socket.getInputStream();
			OutputStream sout = socket.getOutputStream();

			// ������������ ������ � ������ ���, ���� ����� ������������
			// ��������� ���������.
			DataInputStream in = new DataInputStream(sin);
			DataOutputStream out = new DataOutputStream(sout);
			while (true) {
				if (fromDAL.equals("")) {
					String line = in.readUTF();
					BLLClient bc = new BLLClient(line);
					System.out.println("BLL got : " + line);
				} else {
					out.writeUTF(fromDAL);
					out.flush();
				}
			}
		} catch (Exception x) {
			x.printStackTrace();
		}
	}
}
