package com.API;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.invoke.MethodHandles;
import java.net.Socket;

public class ClientK {
	private static void usage() {
		String className = MethodHandles.lookup().lookupClass().getName();
		System.out.printf("Usage: java %s host port%n", className);
	}
	
	private static final String CLIENT_STOP_SIGNAL = "exit";

	private final String host;

	private final int port;

	public ClientK(String host, int port) throws IOException {
		this.host = host;
		this.port = port;
	}

	public void start() {
		// ��������� ���������� � ��������
		try (Socket socket = new Socket(host, port);

				// �������� ���������� ����� �����
			 	BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

				// �������� ������� ����� �� ������� "client <== server"
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

				// �������� �������� ����� �� �������: "client ==> server"
				PrintStream out = new PrintStream(socket.getOutputStream(), true)) {

//			System.out.print("Type your name: ");
//			String name = stdIn.readLine();
//			System.out.printf("Hi %s! You can type your messages below..%n", name);
//			System.out.printf("Type '%s' to quit%n", CLIENT_STOP_SIGNAL);
//			System.out.println("=================");

//			out.println(name);

			// � ��������� ������ ������ ������ � ������� � ����� � �������
			new Worker(in).start();
			new Sender(out).start();
			// ������ ������ �� ������� ������� � �������� �� ������
			String line = null;
			while (!(new Sender().line.equals(""))) {

				// ��������� ������, ���� ������ ��������� stop signal
				if (CLIENT_STOP_SIGNAL.equals(new Sender().line)) {
					break;
				}
				out.printf("[%s] %s%n", "Client", new Sender().line);
				new Sender(out).line="";
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	// ������ ������ � ������� � ����� � ������� �������
	private class Worker extends Thread {

		private BufferedReader in;

		public Worker(BufferedReader in) {
			this.in = in;
		}

		public void run() {
			String line = null;
			try {

				// � ����� ������ ������ � ������� � �������� �� �
				// �������, ������� �� �����, ����� ����� ������ ����� (��. ����
				// try ������ Client#start)
				while ((line = in.readLine()) != null) {

					// �������� �� � �������
					System.out.println(line);
				}
			} catch (IOException e) {

				// ���� ��������, �����
				System.out.println("[Client] has been stopped");
			}
		}
	}

	private static class Sender extends Thread {
		public static String line= "";
		
		private PrintStream out;

		public Sender(){}
		
		public Sender(PrintStream out) {
			this.out = out;
		}

		public void SetLine(String line){
			this.line = line;
		}
		
		public void run() {
			try {
				while (!line.equals("")) {
                    out.println(line);
					System.out.println("I sent "+line);
					line="";
				}
			} catch (Exception e) {
				System.out.println("[Client] has been stopped");
			}
		}
	}

	
	
	public static void main(String[] args) throws IOException {
//		if (args.length != 2) {
//			usage();
//			return;
//		}
        //String inp=args[0];     
		
		String inp="Login/name";  
		
        new Sender().SetLine(inp);
        
		String host = "localhost";
		int port = 55555;

		new ClientK(host, port).start();
	}
}
