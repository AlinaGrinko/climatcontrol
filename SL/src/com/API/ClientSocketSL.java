package com.API;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class ClientSocketSL implements Runnable{
	static private Socket connection;
	static private ObjectOutputStream output;
	static private ObjectInputStream input;
	
	public static void main(String[] args){
		new Thread(new ClientSocketSL("nnn")).start();
		new Thread(new ServerSocketSL()).start();
	}
	
	public ClientSocketSL(String t){
		sendData(t);
	}
	
	@Override
	public void run() {
		try{
			while (true){
				connection=new Socket(InetAddress.getByName("127.0.0.1"),5678);
				output=new ObjectOutputStream(connection.getOutputStream());
				input = new ObjectInputStream(connection.getInputStream());
				System.out.println(output);
			}
		}
		catch(Exception e){}
	}
	
	private static void sendData(Object obj){
		try{
			output.flush();
			output.writeObject(obj);
		} catch(Exception e){}
	}
}
