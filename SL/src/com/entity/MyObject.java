package com.entity;

public class MyObject {

	private String name;
	private String descr;
	private int curTemp;
	private int fromTemp;
	private int toTemp;
	
	public String getName(){
		return this.name;
	}
	public String getDescr(){
		return this.descr;
	}
	public int getCurTemp(){
		return this.curTemp;
	}
	public int getFromTemp(){
		return this.fromTemp;
	}
	public int getToTemp(){
		return this.toTemp;
	}
	
	public void setName(String name){
		this.name=name;
	}
	public void setDescr(String descr){
		this.descr=descr;
	}
	public void setCurTemp(int ct){
		this.curTemp=ct;
	}
	public void setFromTemp(int ft){
		this.fromTemp=ft;
	}
	public void setToTemp(int tt){
		this.toTemp=tt;
	}
	
	public MyObject(){
		this.name="";
		this.descr="";
		this.curTemp=0;
		this.fromTemp=0;
		this.toTemp=0;
	}
	
	public MyObject(String name, String descr, int fromTemp, int toTemp, int curTemp){
		this.name=name;
		this.descr=descr;
		this.curTemp=curTemp;
		this.fromTemp=fromTemp;
		this.toTemp=toTemp;
	}
}
